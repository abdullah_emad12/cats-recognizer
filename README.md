# Cat Classifier

A cat classifier built in python that recognizes cat images with 70% accuracy.

## Disclaimer 
A lot of the code is based on the [Neural Network and Deep Learning](https://www.coursera.org/learn/neural-networks-deep-learning/home/welcome) course on coursera



# Dependencies 

To run the program you need to have the following libraries installed 

1. Numpy
2. h5py
3. matplotlib
4. PIL and scipy

In addition you will need to run it using python3

# Getting Started 

In order to run the code on your own image run the following command from the
terminal in the directory of this repo: 

`python3 creg.py /path/to/your/image.jpg`

where /path/to/your/image is the path to the image you want to recognize
